
public class Main {

	public static void main(String[] args) {
		new Main();

	}

	public Main() {
		// no_1();
		// no_2();
		// no_3();
		no_4();
	}

	// No. 1
	public void no_1() {
		int a = 10;
		int b = 9;
		int tempA = 1;
		int tempB = 1;

		do {
			System.out.print(a + " ");
			System.out.print(b + " ");
			a += tempA;
			b -= tempB;
		} while (a <= 15 && b >= 5);

	}

	// No. 2
	public void no_2() {
		int a = 1;
		int b = 2;
		int square = 2;

		do {
			System.out.println(a);
			System.out.println((int) Math.pow(b, square));
			a += 2;
			b += 2;
			square += 1;
		} while (a <= 7);

	}

	// No. 3
	public void no_3() {
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(i);
			}
			System.out.println("");
		}
	}

	// No. 4
	public void no_4() {
		for (int i = 1; i <= 5; i++) {
			for (int j = 5; j >= i; j--) {
				System.out.print("*");
			}
			System.out.println("");
		}
	}

	// No. 5
	public void number5() {
		int n = 5; // total print *
		int space = n / 2, stNumb = 1;

		// loop for number of lines
		for (int i = 1; i <= n; i++) {
			// loop for printing space
			for (int j = 1; j <= space; j++) {
				System.out.print(" ");
			}

			// loop for printing number
			for (int k = 1; k <= stNumb; k++) {
				System.out.print("*");
			}

			// To goto next line
			System.out.println();
			if (i <= n / 2) {
				space = space - 1;
				stNumb = stNumb + 2;
			} else {
				space = space + 1;
				stNumb = stNumb - 2;
			}
		}
	}

}
